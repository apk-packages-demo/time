set -o errexit -o nounset -o noglob -o pipefail
command -v shopt > /dev/null && shopt -s failglob
(
    . /etc/os-release
    echo \
        $PRETTY_NAME \
        ${VERSION_ID:+$VERSION_ID}
)

gw () {
    s="$@"
    printf \
        "\e[90;47m%s\e[0m\n" \
        "$s"
}
oh () { true ; }
title () {
    s="$@"
    printf \
        "\e[1;4;36m%s\e[0m\n" \
        "$s"
}


# export TERM="xterm-color"
# ...
